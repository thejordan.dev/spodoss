from src import spotify, helper
import json, sys

CONFIG = json.loads(open("./config/config.json","r+").read())

SPOTIFY = CONFIG["spotify_path"]
SONGS = CONFIG["song_link"]
TOKENS = json.loads(open("./data/profiles.json", "r+", encoding="utf-8").read())["credentials"]

def main():
	if (len(sys.argv)>1):
		prompt_action(sys.argv[1])
	else:
		prompt_action()

def prompt_action(ans=None):
	answers = ["1","2","3"]
	answer = ""
	if (ans == None):
		answer = input("Voulez-vous: [1] Faire des comptes  [2] Utiliser les comptes pour streamer  [3] Quitter\n")
	else:
		answer = ans
	if (answer not in answers):
		prompt_action()	
		return
	if (answer == "1"):
		amount = int(input("Combien de comptes voulez-vous créer ?\n"))
		i = 0
		while i < amount: 
			profile = helper.UserProfile()
			credentials = profile.create()
			if (len(credentials) == 2):
				print(credentials[0],credentials[1])
				i += 1
		print(f"{amount} compte(s) ont été créés")
	elif (answer == "2"):
		spotify.farm()
		return
	elif (answer == "3"):
		exit()
	prompt_action()
	return

if __name__ == "__main__":
	main()