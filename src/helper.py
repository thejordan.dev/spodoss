import random, string, randominfo, requests, json
from datetime import datetime

class UserProfile:

    def __init__(self,data=None) -> None:
        if (data == None):
            self.setupNone()
        else:
            self.setupWithData(data)
        pass

    def setupNone(self) -> None:
        self.id = ""
        self.gender = "male" if random.randint(0, 1) else "female"
        self.firstName = randominfo.get_first_name(self.gender)
        self.lastName = randominfo.get_last_name()
        self.email = randominfo.get_email()
        self.password = randominfo.random_password(12,True,True)
        birth_date = randominfo.get_birthdate(18)
        self.birth_date = datetime.strptime(birth_date, "%d %b, %Y")
        self.username = self.firstName.lower()+self.lastName.capitalize()+str(self.birth_date.year)

    def setupWithData(self,data) -> None:
        self.id = data["id"]
        self.gender = data["gender"]
        self.firstName = data["firstName"]
        self.lastName = data["lastName"]
        self.email = data["email"]
        self.password = data["password"]
        self.birth_date = datetime.strptime(data["birthDate"], "%d/%m/%Y")
        self.username = data["username"]

    def header(self):
        
        return {
            "Accept-Encoding": "gzip",
            "Accept-Language": "en-US",
            "App-Platform": "Android",
            "Connection": "Keep-Alive",
            "Content-Type": "application/x-www-form-urlencoded",
            "Host": "spclient.wg.spotify.com",
            "User-Agent": "Spotify/8.6.72 Android/29 (SM-N976N)",
            "Spotify-App-Version": "8.6.72",
            "X-Client-Id": random_string(32)
        }

    def payload(self):
        return {
            "gender": self.gender,
            "displayname": "".lower()+"".capitalize(),
            "email": self.email,
            "password": self.password,
            "password_repeat": self.password,
            "birth_year": self.birth_date.year,
            "birth_month": self.birth_date.month,
            "birth_day": self.birth_date.day,
            "iagree": "true",
            "key": "142b583129b2df829de3656f9eb484e6",
            "creation_point": "client_mobile",
            "platform": "Android-ARM",
        }

    def create(self) -> list[str]:
        req = requests.post("https://spclient.wg.spotify.com/signup/public/v1/account/",headers=self.header(),data=self.payload())
        _json = req.json()
        if (_json["status"] == 1):
            userId = _json["username"]
            if (userId != ""):
                self.id = userId
                log(f"Utilisateur {self.username} créé avec l'id {userId}")
                current = None
                with open("./data/profiles.json", "r+") as tokens:
                    current = json.loads(tokens.read())
                    current["credentials"].append(self.__json__())
                with open("./data/profiles.json", "r+") as tokens:
                    tokens.write(json.dumps(current,indent=4))
                return [userId,self.password]
        log(f"Echec de la création du compte {self.username}")
        return []

    def __json__(self):
        return {
            "id":self.id,
            "gender": self.gender,
            "firstName": self.firstName,
            "lastName": self.lastName,
            "email": self.email,
            "password": self.password,
            "birthDate": self.birth_date.strftime("%d/%m/%Y"),
            "username": self.username
        }

'''
self.gender = "male" if random.randint(0, 1) else "female"
        self.firstName = randominfo.get_first_name(self.gender)
        self.lastName = randominfo.get_last_name()
        self.email = randominfo.get_email()
        self.password = randominfo.random_password(12,True,True)
        birth_date = randominfo.get_birthdate(18)
        self.birth_date = datetime.strptime(birth_date, "%d %b, %Y")
        self.username = self.firstName.lower()+self.lastName.capitalize()+str(self.birth_date.year)
'''

def strike(text):
    return ''.join([u'\u0336{}'.format(c) for c in text])

def log(text):
    print(f"[{get_time()}] - {text}")

def get_time() -> str:
    x = datetime.now()
    return x.strftime("%H:%M:%S")

def random_string(length) -> str:
    pool = string.ascii_lowercase + string.digits
    return "".join(random.choice(pool) for i in range(length))

def random_int(min,max):
    return random.randint(min,max)

def random_text(length) -> str:
    return "".join(random.choice(string.ascii_lowercase) for i in range(length))

def random_list_element(list):
    return random.choice(list)

'''
def make_account(person=random_person("male" if random.randint(0, 1) else "female")):
    return
    payload = 
    pass
'''