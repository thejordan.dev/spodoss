from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as selEc
from selenium.webdriver.common.by import By as selBy
from selenium.webdriver.support.ui import WebDriverWait
import json, time, threading, random, sys

links = dict(
    default = "https://open.spotify.com/playlist/7bvmgNjz8OKEuYwoQs0PCW?play"
)

xpaths = dict(
    login_btn =    "//*[@id='main']/div/div[2]/div[1]/header/div[5]/button[2]",
    user_form =    "//*[@id='login-username']",
    pass_form =    "//*[@id='login-password']",
    cookie_check = "//*[@id='onetrust-accept-btn-handler']",
    submit_btn =   "//*[@id='login-button']",
    shuffle_btn =  "//*[@id='main']/div/div[2]/div[2]/footer/div/div[2]/div/div[1]/div[1]/button[1]",
    repeat_btn =   "//*[@id='main']/div/div[2]/div[2]/footer/div/div[2]/div/div[1]/div[2]/button[2]",
    play_btn =     "//*[@id='main']/div/div[2]/div[2]/footer/div[1]/div[2]/div/div[1]/button",
    main_play_btn ="//*[@id='main']/div/div[2]/div[3]/div[1]/div[2]/div[2]/div/div/div[2]/main/div/section/div[2]/div[2]/div[4]/div/div/div/div/div/button",
    song_name =    "//*[@id='main']/div/div[2]/div[2]/footer/div/div[1]/div/div[2]/div[1]/div/div/div/div/span/a",
    time_track =   "//*[@id='main']/div/div[2]/div[2]/footer/div/div[2]/div/div[2]/div[1]",
    skip_btn =     "//*[@id='main']/div/div[2]/div[2]/footer/div/div[2]/div/div[1]/div[2]/button[1]"
)

def run(username, password, _url, _headless):

    options = Options()
    if (_headless):
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--start-maximized")
        options.add_argument("--autoplay-policy=no-user-gesture-required")
    options.add_argument("--mute-audio")
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    browser = Chrome(options=options)
    wait = WebDriverWait(browser, 10)
    browser.get(_url) if _url else browser.get(links["default"])

    # Login with given credentials
    login_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["login_btn"])))
    login_btn.click()
    user_form = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["user_form"])))
    user_form.send_keys(username)
    pass_form = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["pass_form"])))
    pass_form.send_keys(password)
    submit_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["submit_btn"])))
    submit_btn.click()

    # Accept Cookies
    cookie_check = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["cookie_check"])))
    cookie_check.click()

    play_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["main_play_btn"])))
    if ("Lire" in play_btn.get_attribute("aria-label")):
        play_btn.click()

    shuffle_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["shuffle_btn"])))
    repeat_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["repeat_btn"])))

    # Activate shuffle if disabled
    if "Activer la lecture aléatoire" in shuffle_btn.get_attribute("aria-label"):
        time.sleep(1)
        shuffle_btn.click()

    # Set repeat mode to "playlist"

    if "tP0mccyU1WAa7I9PevC1" in repeat_btn.get_attribute("class"):
        if "Autoriser la répétition" in repeat_btn.get_attribute("aria-label"):
            pass
        else:
            time.sleep(1)
            repeat_btn.click()
            time.sleep(1)
            repeat_btn.click()
    else:
        time.sleep(1)
        repeat_btn.click()

    skip_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["skip_btn"])))
    skip_btn.click()

    while True:
        time.sleep(random.randint(55, 70))
        song_name = wait.until(selEc.presence_of_element_located((selBy.XPATH, xpaths["song_name"]))).text
        time_track = wait.until(selEc.presence_of_element_located((selBy.XPATH, xpaths["time_track"]))).text
        print(" * Played {0} for {1}".format(song_name, time_track))
        skip_btn = wait.until(selEc.element_to_be_clickable((selBy.XPATH, xpaths["skip_btn"])))
        skip_btn.click()


def farm():
    selUrl = ""
    if len(sys.argv) < 3 or sys.argv[2] != "default":
        selUrl = input(" * Insert Spotify playlist url (empty for default): ")
    headless = "--headless" in sys.argv
    with open('./data/profiles.json', 'r') as f:
        credentials = json.load(f)
        print(" * Opening browsers...")
        for data in credentials['credentials']:
            threading.Thread(target=run, args=[data['email'], data['password'], selUrl, headless]).start()
            time.sleep(1)
        f.close()